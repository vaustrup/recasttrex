#ifndef UNFOLDING_H
#define UNFOLDING_H

// Unfolding includes
#include "UnfoldingCode/UnfoldingCode/FoldingManager.h"

#include <string>

class Unfolding {
public:

    enum UnfoldingChi2Type {
        EMPTY = 0,
        CHI2 = 1,
        CHI2NDF = 2,
        PROB = 3
    };

    explicit Unfolding();
    Unfolding(const Unfolding& unf) = delete;
    Unfolding& operator=(const Unfolding& unf) = delete;
    Unfolding(Unfolding&& unf) = delete;
    Unfolding& operator=(Unfolding&& unf) = delete;

    ~Unfolding() = default;

    std::string fName;
    FoldingManager::MATRIXORIENTATION fMatrixOrientation;
    std::string fTruthDistributionPath;
    std::string fTruthDistributionFile;
    std::string fTruthDistributionName;
    int fNumberUnfoldingTruthBins;
    double fUnfoldingResultMin;
    double fUnfoldingResultMax;
    std::string fUnfoldingTitleX;
    std::string fUnfoldingTitleY;
    double fUnfoldingScaleRangeY;
    double fUnfoldingTitleOffsetY;
    double fUnfoldingTitleOffsetX;
    double fUnfoldingRatioYmin;
    double fUnfoldingRatioYmax;
    bool fUnfoldingLogX;
    bool fUnfoldingLogY;
    std::string fMigrationTitleX;
    std::string fMigrationTitleY;
    bool fMigrationLogX;
    bool fMigrationLogY;
    double fMigrationTitleOffsetX;
    double fMigrationTitleOffsetY;
    double fMigrationZmin;
    double fMigrationZmax;
    double fResponseZmin;
    double fResponseZmax;
    bool fPlotSystematicMigrations;
    bool fMigrationText;
    std::string fNominalTruthSample;
    std::string fAlternativeAsimovTruthSample;
    bool fUnfoldingDivideByBinWidth;
    double fUnfoldingDivideByLumi;
    UnfoldingChi2Type fUnfoldingChi2Type;
    bool fUnfoldNormXSec;
    int fUnfoldNormXSecBinN;

private:
};

#endif
