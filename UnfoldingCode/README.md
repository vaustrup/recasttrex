# UnfoldingCode [![build status](https://gitlab.cern.ch/TRExStats/unfoldingcode/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/TRExStats/unfoldingcode/commits/master)

This simple package provides utilities for a profile-likelihood unfolding such as folding functionality.

## Getting the code
To get the code simply clone it using the following command:
```
git clone ssh://git@gitlab.cern.ch:7999/TRExStats/unfoldingcode.git
```

## Requirements
The code only needs `cmake` and `ROOT`

## Using the code

### Folding

You need to include the header with
```c++
#include "UnfoldingCode/FoldingManager.h"
```

and declare the `FoldingManager`
```c++
FoldingManager mgr{};
```

Then, you need to set the matrix orientation
```c++
mgr.SetMatrixOrientation(FoldingManager::MATRIXORIENTATION::TRUTHONHORIZONTALAXIS) // or FoldingManager::MATRIXORIENTATION::TRUTHONVERTICALAXIS
```

Now, you need to provide the truth distribution (`TH1*`)

```
mgr.SetTruthDistribution(truth);
```

Then you can either provide the response matrix directly (`TH2*`) or provide selection efficiency (`TH1*`) and migration matrix (`TH2*`) and acceptance matrix (`TH1*`) if relevant

```c++
// passing migration, selection efficiency and acceptance
mgr.SetMigrationMatrix(matrix, false); // second argument tells the code to normalise the matrix
mgr.SetSelectionEfficiency(eff);
mgr.SetAcceptance(acc);

// Calculate the response matrix
mgr.CalculateResponseMatrix(true); // the argument tells the code to overwrite the current response matrix if it exists

// when you have the response matrix directly
mgr.SetResponseMatrix(response);
```

Now you can process the histograms and do the bin-by-bin folding
```c++
mgr.FoldTruth();
```

You can retrive the folded histograms (`std::vector<TH1D>`) with
```c++
std::vector<TH1D> folder = mgr.GetFoldedDistributions();
```

You can also retrieve the relevant histograms with
```c++
mgr.GetTruthDistribution(); // returns const TH1D*
mgr.GetSelectionEfficiency(); // returns const TH1D*
mgr.GetAcceptance(); // returns const TH1D*
mgr.GetMigrationMatrix(); // returns const TH2D*
mgr.GetResponseMatrix(); // returns const TH2D*
```

### Plotting the result

A simple class is provided to calculate the final unfolded distribution.

To use it, you need to include
```c++
#include "UnfoldingCode/UnfoldingResult.h"
```

And then initialise the tool
```c++
UnfoldingResult result{};
result.SetTruthDistribution(truth); // Argument is TH1*
result.SetNormXSec(true); // Tells the code if the result is normalised distribution or not
```

Then you need to add the fitted values of the normalisation factors (mean value, error up, error down)
```c++
result.AddFitValue(mean1, up1, down1);
...
result.AddFitValue(meanN, upN, downN);
```

And to retrieve the results you need to do
```c++
std::unique_ptr<TH1D> unfoldedData       = result.GetUnfoldedResult();
std::unique_ptr<TGraphAsymmErrors> error = result.GetUnfoldedResultErrorBand();

//To dump the results into a text file do
result.DumpResults(file); // where the argument is std::ofstream*
```
