# MC statistical uncertainty treatment

`TRExFitter` automatically creates nuisance parameters to describe the effect of limited Monte Carlo statistics.
These nuisance parameters are also called _gammas_.
See the [HistFactory documentation](https://cds.cern.ch/record/1456844) for more details about their technical implementation in the fit model.
We will highlight a few aspects here regarding the use of gammas in `TRExFitter`, and choices you have during model building.

## Gamma basics

By default a single MC stat constraint term is assigned to each bin in the fit.
The parameters have names `gamma_stat_{REGION_NAME}_bin_{X}` where `{REGION_NAME}` is the name you give to your regions in the config file, and `{X}` is the bin (starting from 0) per region.
All sample yields (other than data) in a bin are scaled multiplicatively by the associated gamma.
The pruning setting `MCstatThreshold` can be used to drop gammas in bins with MC statistical uncertainties below a threshold from the fit model.
By setting `MCstatThreshold: 0.01`, gammas are only included for a bin if the MC statistical uncertainty for this bin is greater than 1%.
`MCstatThreshold: NONE` completely disables all gammas, including those created with `SeparateGammas` (see below).

A Poisson constraint term is used by default for the gamma nuisance parameters.
It is possible to switch to a Gaussian constraint term via the `MCstatConstraint: GAUSSIAN` setting.

Try to limit the size of the statistical uncertainty per bin to at most 20%.
Larger values can bias the signal extraction, see [these slides](https://indico.cern.ch/event/615262/contributions/2484815/) for a study and the corresponding recommendation.

!!! info "Example: ttH config"
    The ttH config example provided in `config/ttH_tutorial.config` can be used to demonstrate the creation of gammas.
    Run a fit via
    ```bash
    trex-fitter nwf config/ttH_tutorial.config
    ```

    and have a look at the fit results summarized in `ttH_tutorial/Fits/ttH_tutorial.txt`.
    A nuisance parameter appears for every bin, with best-fit values around 1.
    The best-fit results for gammas are also visualized in `ttH_tutorial/Gammas.png`:
    ![Gammas](img/mc_stats/Gammas.png)
    As you can see, the MC statistical uncertainty in the second bin of the dilepton region is larger than 20%.
    This limitation arises in this example where the input file size is deliberately kept small.
    In practical applications, avoid this situation by choosing different binning or cuts, or larger inputs!

## Splitting MC statistical uncertainties per sample

In many situations it makes sense to exclude the signal from the same gamma nuisance parameter treatment as the rest of the samples.
Imagine the following situation.
You have a background estimation using very few MC events, and correspondingly a large MC statistical uncertainty.
In contrast to this, the MC statistical uncertainty for signal is typically small, since the analysis is likely optimized to have larger signal acceptance.
If you have a single gamma scaling both background and signal, then a pull of the gamma due to the finite number of background MC events will also be applied to signal equivalently.
The signal MC statistical uncertainty may be much smaller though, and this will bias your signal extraction.
If your signal MC statistical uncertainty is negligible, one possible approach is to not attach gammas to the signal at all, using the `UseMCstat` option.
See the example below for details.

!!! info "Excluding a sample"
    The `config/ttH_tutorial.config` example which we already looked at above uses the `UseMCstat` for the ttH signal.
    We will have a closer look at the workspace to see what exactly happened due to this setting.
    Open a part of the workspace, the `.xml` file describing the 6j4b channel: `ttH_tutorial/RooStats/ttH_tutorial_ljets_HThad_ge6jge4b.xml`.
    One thing you will see is the following line:
    ```xml
    <StatErrorConfig RelErrorThreshold="0.01" ConstraintType="Poisson" />
    ```

    which shows us that the pruning threshold is 1% (corresponding to our `MCstatThreshold` setting), and we are using Poisson constraint terms for our gammas.
    A bit further down we can see the ttH sample block
    ```xml
    <Sample Name="ttH" [...]>
      <OverallSys Name="Lumi"  High="1.05"  Low="0.95"   />
      [...]
    </Sample>
    ```

    in which `OverallSys` and `HistoSys` modifiers appear.
    Have a look at the [HistFactory documentation](https://cds.cern.ch/record/1456844/) to learn more about them.
    Going down to the next sample, which is ttbar, we see
    ```xml
    <Sample Name="ttbar" [...]>
      <StatError Activate="True"  InputFile=""  HistoName=""  HistoPath=""  />
      <OverallSys Name="Lumi"  High="1.05"  Low="0.95"   />
      [...]
    </Sample>
    ```

    which means that the `StatError` modifier (which is expanded by HistFactory into one gamma per bin) acts on the ttbar sample.
    Going down further, you see it also acts on the singleTop samples, but not on the ttH sample due to our `UseMCstat` setting.
    The other two regions in the workspace show the same behavior.

Besides just excluding one or more samples from the MC statistical uncertainty treatment, `TRExFitter` offers another useful option.
The `SeparateGammas` setting can be used to exclude a sample from the automatically generated gammas, but to create a new nuisance parameter that only scales this specific sample.
The implementation makes use of the so-called `ShapeSys` modifiers in HistFactory, which have Poisson constraint terms by default, like the normal gammas.
The constraint term can be switched to Gaussian via the `UseGaussianShapeSysConstraint` setting.

!!! info "Separate constraint terms for a sample"
    We will try the `SeparateGammas` treatment with the `config/ttH_tutorial.config` example.
    Replace the `UseMCstat: FALSE` setting for the ttH sample by `SeparateGammas: TRUE`.
    It is not sufficient to only re-create the workspace for this, you need to run `nwf`:
    ```bash
    trex-fitter nwf config/ttH_tutorial.config
    ```

    Take a look at the fit results in `ttH_tutorial/Fits/ttH_tutorial.txt` and you will notice that new parameters have appeared, with names in the format
    ```txt
    gamma_shape_stat_{SAMPLE}_{REGION}_bin_{X}
    ```

    where `{SAMPLE}` in this case is ttH, and `{REGION}` and `{X}` are the bins for the regions present in the fit.
    These new nuisance parameters scale the ttH sample yields multiplicatively, and also show up in the plot of gammas (`ttH_tutorial/Gammas.png`):
    ![Gammas](img/mc_stats/Gammas_with_SeparateGammas.png)

    As  you can see in this case, the statistical uncertainties for the signal sample are much smaller than those for the background samples.

Have a look at the `CorrelateGammasInRegions` and `CorrelateGammasWithSample` settings as well: those allow you to correlate modifiers created via `SeparateGammas` across regions and between samples, respectively.

## Other related settings

The effect of gammas is **NOT** propagated to post-fit data/MC plots by default.
To change this, use `UseGammaPulls: TRUE`.
This is only possible for regions that were used in the fit: there is no meaningful way to apply the post-fit effect of gammas to validation regions, or any other regions you may wish to project the fit result onto.

The `UseGammasForCorr` setting can be used to include gammas in the correlation matrix, by default this is disabled.

## Uncertainties on uncertainties?

The HistFactory model does not include a method for describing uncertainties on uncertainties.
In particular, if the $\pm 1 \sigma$ template histograms built to evaluate systematic uncertainties are affected by large statistical fluctuations, this effect is not captured at all in the fit.
Try a different binning, relaxed cuts, or other ways to obtain more meaningful systematic templates in this case.
